/*
 Copyright (C) BABEC. All rights reserved.
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"encoding/json"
	"fmt"
	"strings"

	"chainmaker.org/chainmaker/contract-sdk-go/v2/pb/protogo"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/sandbox"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
)

const (
	paramProductId = "productId"
	paramPrice     = "cost"
	paramTo        = "to"
	paramUsers     = "users"
	paramInventory = "inventory"
)

type supplychain interface {
	manufacture(productId, price string) protogo.Response
	purchase(productId, price, to string) protogo.Response

	InitContract() protogo.Response    // return "Init contract success"
	UpgradeContract() protogo.Response // return "Upgrade contract success"
	InvokeContract(method string) protogo.Response
}

var _ supplychain = (*SupplychainContract)(nil)

// SupplychainContract contract
type SupplychainContract struct {
}

// InitContract install contract func
func (e *SupplychainContract) InitContract() protogo.Response {
	return sdk.Success([]byte("Init contract success"))
}

// UpgradeContract upgrade contract func
func (e *SupplychainContract) UpgradeContract() protogo.Response {
	return sdk.Success([]byte("Upgrade contract success"))
}

// InvokeContract the entry func of invoke contract func
func (e *SupplychainContract) InvokeContract(method string) protogo.Response {
	args := sdk.Instance.GetArgs()
	if len(method) == 0 {
		return Error("method of param should not be empty")
	}

	switch method {
	case "manufacture":
		productId := string(args[paramProductId])
		price := string(args[paramPrice])
		return e.manufacture(productId, price)
	case "purchase":
		productId := string(args[paramProductId])
		price := string(args[paramPrice])
		to := string(args[paramTo])
		return e.purchase(productId, price, to)
	default:
		return Error("Invalid method" + method)
	}
}
func (e *SupplychainContract) manufacture(productId, price string) protogo.Response {
	result, err := sdk.Instance.GetStateFromKey(paramUsers)
	if err != nil {
		return sdk.Error("failed to get user list")
	}
	users := strings.Split(result, ",")
	to, err := sdk.Instance.GetSenderAddr()
	if err != nil {
		return sdk.Error("failed to get sender address")
	}
	productId = strings.TrimSpace(productId)
	inventory := map[string]string{} // 该 producer 的库存
	if !stringContains(users, to) {
		if addUser(users, to) != nil {
			return sdk.Error(fmt.Sprintf("failed to add user %s", to))
		}
		inventory[productId] = price
	} else {
		inventory, err = getInventory(to)
		if err != nil {
			return sdk.Error(fmt.Sprintf("failed to get %s producer inventory", to))
		}
		inventory[productId] = price
	}

	err = saveInventory(inventory, to)
	if err != nil {
		return sdk.Error(fmt.Sprintf("failed to save %s inventory", to))
	}

	return sdk.Success([]byte("success"))
}

func addUser(users []string, user string) error {
	return sdk.Instance.PutStateFromKey(paramUsers, strings.Join(append(users, user), ",")) // 新增一个 user
}

func getInventory(address string) (map[string]string, error) {
	inventory := map[string]string{} // 该 producer 的库存
	result2, err := sdk.Instance.GetStateByte(paramInventory, address)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(result2, &inventory)
	if err != nil {
		return nil, err
	}
	return inventory, nil
}

func saveInventory(inventory map[string]string, address string) error {
	// 存货上链
	inventoryBytes, err := json.Marshal(inventory)
	if err != nil {
		return err
	}
	err = sdk.Instance.PutStateByte(paramInventory, address, inventoryBytes)
	if err != nil {
		return err
	}
	return nil
}

func (e *SupplychainContract) purchase(productId, price, to string) protogo.Response {
	result, err := sdk.Instance.GetStateFromKey(paramUsers)
	if err != nil {
		return sdk.Error("failed to get users list")
	}
	users := strings.Split(result, ",")
	to = strings.TrimSpace(to)
	from, err := sdk.Instance.GetSenderAddr() // 权限控制：发起者为产品持有者
	if err != nil {
		return sdk.Error("failed to get sender address")
	}
	if !stringContains(users, from) {
		return sdk.Error(fmt.Sprintf("user %s not exist", from))
	}
	productId = strings.TrimSpace(productId)
	fromInventory, err := getInventory(from) // 该 from 的库存
	if err != nil {
		return sdk.Error(fmt.Sprintf("failed to get %s inventory", from))
	}
	if _, ok := fromInventory[productId]; !ok {
		return sdk.Error(fmt.Sprintf("%s has no product %s", from, productId))
	} else {
		// 移除这个人的productId存货
		delete(fromInventory, productId)
	}

	// transfer
	toInventory := map[string]string{}
	if !stringContains(users, to) {
		if addUser(users, to) != nil {
			return sdk.Error(fmt.Sprintf("failed to add user %s", to))
		}
		toInventory[productId] = price
	} else {
		toInventory, err = getInventory(to)
		if err != nil {
			return sdk.Error(fmt.Sprintf("failed to get %s inventory", to))
		}
		toInventory[productId] = price
	}

	err = saveInventory(fromInventory, from)
	if err != nil {
		return sdk.Error(fmt.Sprintf("failed to save %s inventory", from))
	}
	err = saveInventory(toInventory, to)
	if err != nil {
		return sdk.Error(fmt.Sprintf("failed to save %s inventory", to))
	}

	return sdk.Success([]byte("success"))
}

// Error return error response with message
func Error(message string) protogo.Response {
	return protogo.Response{
		Status:  sdk.ERROR,
		Message: "[supplychain] " + message,
	}
}

func main() {
	err := sandbox.Start(new(SupplychainContract))
	if err != nil {
		sdk.Instance.Errorf(err.Error())
	}
}
