package main

func stringContains(ss []string, s string) bool {
	for _, tmp := range ss {
		if tmp == s {
			return true
		}
	}
	return false
}
