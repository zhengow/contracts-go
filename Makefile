
build:
	cd erc20 && ./build.sh erc20
	cd erc721 && ./build.sh erc721
	cd identity && ./build.sh identity
	cd exchange && ./build.sh exchange
	cd fact && ./build.sh fact
	cd raffle && ./build.sh raffle
	cd encdata && ./build.sh encdata
	cd vote && ./build.sh vote

lint:
	cd erc20 && golangci-lint run ./...
	cd erc721 && golangci-lint run ./...
	cd identity && golangci-lint run ./...
	cd exchange && golangci-lint run ./...
	cd fact && golangci-lint run ./...
	cd raffle && golangci-lint run ./...
	cd encdata && golangci-lint run ./...
	cd vote && golangci-lint run ./...

gomod:
	cd erc20 && go get chainmaker.org/chainmaker/contract-sdk-go/v2@v2.3.2 && go get chainmaker.org/chainmaker/contract-utils@v1.0.0 && go mod tidy
	cd erc721 && go get chainmaker.org/chainmaker/contract-sdk-go/v2@v2.3.2 && go get chainmaker.org/chainmaker/contract-utils@v1.0.0 && go mod tidy
	cd identity && go get chainmaker.org/chainmaker/contract-sdk-go/v2@v2.3.2 && go get chainmaker.org/chainmaker/contract-utils@v1.0.0 && go mod tidy
	cd exchange && go get chainmaker.org/chainmaker/contract-sdk-go/v2@v2.3.2 && go mod tidy
	cd fact && go get chainmaker.org/chainmaker/contract-sdk-go/v2@v2.3.2 && go mod tidy
	cd raffle && go get chainmaker.org/chainmaker/contract-sdk-go/v2@v2.3.2 && go mod tidy
	cd encdata && go get chainmaker.org/chainmaker/contract-sdk-go/v2@v2.3.2 && go get chainmaker.org/chainmaker/common/v2@v2.3.0 && go mod tidy
	cd vote && go get chainmaker.org/chainmaker/contract-sdk-go/v2@v2.3.2 && go get chainmaker.org/chainmaker/common/v2@v2.3.0 && go mod tidy
